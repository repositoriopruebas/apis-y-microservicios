import React from 'react';
import { Table } from "react-bootstrap";
import Button from '@material-ui/core/Button';
import { deleteTasks } from '../services/TaskService';


class TaskRow extends React.Component
{
    constructor(props)
    {
        super(props);
    }
    state = {
        open:false
    }
    hangClick(event)
    {      
        deleteTasks(this.props.ident);
        this.props.delete();
    }
    editClick(event)
    {
        this.setState( {open: !this.state.open} )
        this.props.modal(this.props);
    }
    render()
    {              
        return (                              
                    <tbody>
                        <tr>
                            <td>{this.props.ident}</td>
                            <td>{this.props.titulo}</td>
                            <td>{this.props.url}</td>
                            <td>{this.props.description}</td>
                            <td><Button onClick={ this.editClick.bind(this) } variant="contained" color="primary" href="#contained-buttons">
                                    Editar
                                </Button>
                                <Button onClick={ this.hangClick.bind(this) } variant="contained" color="secondary" href="#contained-buttons">
                                    Eliminar
                                </Button>
                            </td>
                        </tr>                           
                    </tbody>                         
            );            
    }    
}
export default TaskRow;



