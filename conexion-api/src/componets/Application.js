import React from 'react';
import Formulary from './Formulary';
import TaskRow from './TasksComponent';
import { getTasks } from '../services/TaskService';
import { Table } from "react-bootstrap";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Spinner } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.css';


class Application extends React.Component
{
    constructor(props)
    {
        super(props);    
        this.state = {
            tasks:[],
            empty:false,
            isOpen:false,
            loadingInsert:false,
            loadingDom:false,
            ident:'',
            title:'',
            url:'',
            description:'',
            toastState: false
        }
    }
    async componentDidMount()
    {      
        try 
        {
            var tasks = await getTasks(); 
           
            if(tasks.length == 0)
            {                
                this.setState({empty:true});
                this.setState( {loadingDom: true} );  
            }else
            {
                this.setState( {loadingDom: true} );  
                this.setState({empty:false});
                this.setState({tasks:tasks}); 
                            
            }                    
        } catch (error) 
        {
            console.log(error);
        }     
    } 

    async save()
    {   
        var tasks = await getTasks();       
        this.setState({tasks:tasks});
        if(this.state.tasks.length == 0)
            {                
                this.setState({empty:true});                
            }else
            {
                this.setState({empty:false});                 
                this.setState({toastState:true})
                this.successAlerts();          
            }            
    }
    async delete()
    {           
        if(window.confirm("¿Confirma ud que desea borrar el registro? "))
        {
            var tasks = await getTasks();       
            this.setState({tasks:tasks});
            if(this.state.tasks.length == 0)
            {
                this.setState({empty:true});         

            }
        }       
    }
    modalControllerOpen(o)
    {
        this.setState({ isOpen: true } );
        this.setState({ ident:o.ident } );
        this.setState({ title:o.titulo } );
        this.setState({ url:o.url});
        this.setState({ description:o.description})
    }
    modalControllerClose(e)
    {
        this.setState( { isOpen: false } )
    }
    validateAlerts()
    {
        toast.error("Algunos de los campos están vacíos"); 
    }
    successAlerts()
    {
       return toast.success("Registro introducido con éxito"); 
      
    }
    async refreshAfterUpdate(e)
    {
        var tasks = await getTasks();       
        this.setState({tasks:tasks});
        this. modalControllerClose(e)
    }

    render()
    {     
        const empty = "No hay datos en la API tuuu   ";
        const nullable = "";
        return (<div>  
                    {(this.state.loadingInsert == true) ? <div style={{ width: '3rem', height: '3rem', margin:'auto'  }}> <Spinner style={{ width: '3rem', height: '3rem', textAlign: 'center', margin:'50%'}} />  </div>: <div style={{ width: '3rem', height: '3rem',  }}> </div>} 
                    { (this.state.toastState == true) ?  <ToastContainer />  : "" }
                                               
                   < Formulary mode={"insert" } save={this.save.bind(this)}   validate={this.validateAlerts.bind(this)}/> 
                 
                    {(!this.state.loadingDom) ? <Spinner style={{ width: '3rem', height: '3rem' }} /> : 
                         < Table data={this.props.data} striped bordered hover >         
                         <thead>
                                 <tr>
                                     <th>id</th>
                                     <th>Título</th>
                                     <th>Url</th>
                                     <th>Descripción</th>
                                     <th>Acciones</th>
                                 </tr>
                         </thead>
                             { (this.state.empty)? empty : this.state.tasks.map((value, index) => (                 
                                 < TaskRow modal={this.modalControllerOpen.bind(this)} delete={this.delete.bind(this)} key={value.id} ident={value.id} url={value.url} description={value.description} titulo={value.title} />                               
                                 )) }   
                        </ Table >                          
                    }

                    <Modal  isOpen= { this.state.isOpen } >
                            <ModalHeader>
                                Editar Registro    
                            </ModalHeader>
                            <ModalBody>
                                < Formulary mode={"update" } update={this.refreshAfterUpdate.bind(this)} idt={this.state.ident} titulo={this.state.title} url={this.state.url} description={this.state.description}/>   
                            </ModalBody>
                            <ModalFooter>
                                    <Button onClick={this.modalControllerClose.bind(this)}> Cerrar </Button> 
                            </ModalFooter>
                    </Modal>  
                </div>);
    }    
}
export default Application;