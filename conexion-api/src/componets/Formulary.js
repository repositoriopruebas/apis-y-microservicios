import React from 'react';
import { Form, Col, Button, Row } from "react-bootstrap";
import {insertTask} from '../services/inputTaskService';
import {updateTask} from '../services/UpdateService';

class Formulary extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            id: '',
            title:'',
            url:'',
            description:'',
            captionButton:'Enviar',
            colorButton:'p-2 btn btn-success',
            loading:false,
            tasks:{}
        }
        this.titleHandler = this.titleHandler.bind(this);
        this.urlHandler = this.urlHandler.bind(this);
        this.descriptionHandler = this.descriptionHandler.bind(this);
    }
    titleHandler(e)
    {
        
        this.setState({title:e.target.value})  
       
     
        
    }
    urlHandler(e)
    {
        this.setState({url:e.target.value})
    }
    descriptionHandler(e)
    {
        this.setState({description:e.target.value})        
    }
    componentDidMount()
    {
        if(this.props.mode == "update")
        {
            this.setState({captionButton:'Editar Registro'});
            this.setState({colorButton:"p-2 btn btn-primary"})
        }  
    }
    async insertFunction()
    {      
        try 
        {
            const tasks = await insertTask(this.state); 
            if(tasks == '')
            {
                this.props.validate()
            }else
            {
                this.setState({tasks:tasks});         
                this.props.save(); 
              //  this.successAlerts();
               
            }                
        } catch (err) {
            console.error(err);       
        }
    }
  async updateFunction(e)
    {
      
       this.setState( {id:this.props.idt});
       const task = {
           id:this.props.idt,
           title:this.state.title,
           url:this.state.url,
           description:this.state.description

       } 
      await updateTask(task);
    }

    handleSubmit(e)
    {     
        e.preventDefault();        
        if(this.props.mode == "insert")
        {
            this.setState( {loading:true} );          
            this.insertFunction();
       
        }else if(this.props.mode == "update")
        {
            this.updateFunction(e);
            this.props.update()
        } 
       

    }
    render()
    {
        return(
                <div>   
                                 
                    <Form style= {{ width: 1200 }} >
                    <Form.Group as={Row} controlId="formPlaintextEmail">                     
                        <Form.Label column sm="2">
                            Titulo
                        </Form.Label>
                        <Col sm="2">
                        <Form.Control name="title" onChange={this.titleHandler}  placeholder={this.props.titulo} autofocus="true" />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formPlaintextPassword">
                        <Form.Label column sm="2">
                        url
                        </Form.Label>
                        <Col sm="2">
                            <Form.Control name="url" onChange={this.urlHandler} placeholder={this.props.url} type="text"  />                      
                        </Col>                   
                    </Form.Group>
                    <Form.Group as={Row} name="description"  onChange={this.descriptionHandler} controlId="formPlaintextButton">
                        <Form.Label column sm="2">
                            Descripción
                        </Form.Label>
                        <Col sm="2">
                            <Form.Control name="description" as="textarea"   placeholder={this.props.description} />
                            <input type="submit" className={this.state.colorButton} value={this.state.captionButton} onClick={this.handleSubmit.bind(this)}/>
                        </Col> 
                    </Form.Group>
                </Form>            
            </div>
                
        )
    }
}
export default Formulary;