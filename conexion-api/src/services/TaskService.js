export async function getTasks()
{
    const response = await fetch('http://localhost:4000/links',{
        method: 'GET',
        headers: new Headers({ 'Access-Control-Allow-Origin': '*'}),
        mode: 'cors'
    });
    const responseJson = await response.json();
    return responseJson
}

export async function deleteTasks(id)
{
    const response = await fetch('http://localhost:4000/links/erase/'+id,{
        method: 'DELETE',
        headers: new Headers({ 'Access-Control-Allow-Origin': '*'}),
        mode: 'cors'
    });
    const responseJson = await response.json();
    return responseJson
}
