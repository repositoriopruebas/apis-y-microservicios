export async function updateTask(task)
{
   if(task.title == '' || task.url == '' || task.description == '')
   {
      
      return 'no han llegado los parámetros';
   }else
   {
        let res = await fetch('http://localhost:4000/links/update/'+task.id,{
            method: 'PUT',
            headers: {
                "Accept":"application/json",
                "Content-Type":"application/json",
                "Access-Control-Allow-Origin": "*"      
    },
            body:JSON.stringify(task),
      });
      let json = await res.json();
     
      return json;
   }  
}