const connection = require('./dbconf');

connection.connect((err) =>
{
    if(err)
    {
        console.error('error de conexion '+err.stack)
    }else
    {
        console.log("conectado a la base de datos "+connection.state)
    }

});

module.exports = connection;