const express = require('express');

const router = express.Router();
module.exports = router;
const pool = require('../database');

router.get('/add', (req,res) => {
   res.render('./links/add');
});
router.post('/add', async (req,res) => {
    console.log(req.body);
    const { title, url, description} = req.body;
    const newLink = 
    {
        title,
        url,
        description
    };
    await pool.query('INSERT INTO links set ?',([newLink]),async function(error){
        if(error)
        {
            console.log(error.message);
        }else
        {
            console.log('success');
            const links = await pool.query('SELECT * FROM links');
            res.json(links)    
        }
    })   
    console.log(newLink);
});
router.get('/',async (req,res) => {
    const links = await pool.query('SELECT * FROM links');
    res.json(links)
    
})
router.delete('/erase/:_id', async (req,res) => {
    console.log(req.params._id);
    const resp = await pool.query('DELETE FROM links WHERE id= ?',req.params._id, (err) => {
        if(err)
        {
           res.json({error : err});
        }else
        {
            res.json({success : true});
        } 
       
    })
});
router.put('/update/:id', async (req,res) => {
    console.log(req.body);
    const resp = await pool.query('UPDATE links SET ? WHERE id = ?', [req.body,  req.body.id], (error, result) => {
        if (error) {
            res.json( error);
        }

       res.json("subido");
       
    });
});
module.exports = router;