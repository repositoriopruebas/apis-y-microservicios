const express = require('express');
const morgan = require('morgan');
const router = require('./routes');
const authentication = require('./routes/authentication');
const links = require('./routes/links');
const exphbs = require('express-handlebars');
const path = require('path');
var cors = require('cors')
// initalizations
const app = express();
app.use(cors());
//settings
app.set('port',process.env.PORT || 4000);
app.set('views', path.join(__dirname, 'views' ));
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layout'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
    helper: require('./lib/handlebars')
}));
app.set('view engine', '.hbs')
//middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended:false}));
app.use(express.json());

//globals variables
app.use((req, res, next) =>{
    next();    
})

//routes
app.use(router);
app.use(authentication);
app.use('/links',require('./routes/links'));
//public

//starting the server

app.listen(app.get('port'), () => {
    console.log('Server on port '+app.get('port')); 
});

