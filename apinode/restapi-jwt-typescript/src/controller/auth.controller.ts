import {Request, Response} from 'express'
import User, { IUser } from '../models/User'
import jwt from 'jsonwebtoken'
export const signup = async (req: Request, res: Response) => 
{
    //saving new user
    const user: IUser = new User(
        {
            username: req.body.username,
            email : req.body.email,
            password: req.body.password
        }
    );
    user.password = await user.encryptPassword(user.password);
    const userSaved = await user.save();
    console.log(userSaved);    
    // token
    const token = jwt.sign({ id: userSaved.id}, process.env.TOKEN_SECRET || 'tokentest');
    res.header('auth-token', token).json(userSaved);
}
export const signin = async (req: Request, res: Response) => 
{
    const user = await User.findOne({email:req.body.email})
    if(!user) return res.status(400).json("email o password incorrectos");
    const correctPassword:boolean = await user.validatePassword(req.body.password);
    if(!correctPassword)
    {       
        return res.status(400).json(" password incorrectos");
    }else
    {
        const token:string = jwt.sign({ id: user.id}, process.env.TOKEN_SECRET || 'tokentest', {
            expiresIn: 600 * 60 * 24
        });
        res.header('auth-token', token).json(user)
    }
    
}
export const profile = async (req: Request, res: Response) =>
{
    console.log("    ******************* "+req.userId);
    const user = await User.findById(req.userId);
    if(!user) 
    {
        return res.status(404).json("No User Found")
    }else
    {
        res.json(user);
    }
    
}